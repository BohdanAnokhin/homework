package DataUpdate;

import java.sql.DriverManager;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Locale;

public class DataUpdate {
	static final String JDBC_DRIVER = "oracle.jdbc.driver.OracleDriver";  
	static final String DB_URL = "dbc:oracle:thin:@localhost:1521:XE";
	static final String USER = "USER_ZONA_51";
	static final String PASS = "masterzonu51";
	   
	   public static void main(String[] args) {
	   Connection conn = null;
	   Statement stmt = null;
	   try{
	      
	      Class.forName("oracle.jdbc.driver.OracleDriver");
	      Locale.setDefault(Locale.ENGLISH);
	      
	      System.out.println("Connecting to a selected database...");
	      conn = DriverManager.getConnection(DB_URL, USER, PASS);
	      System.out.println("Connected database successfully...");
	      
	      System.out.println("Inserting records into the table...");
	      stmt = conn.createStatement();
 //UPDATE      
	/*     String DBUSER = "UPDATE DBUSER " +
                  "SET age = 30 WHERE id in (2)";
	      stmt.executeUpdate(DBUSER);
 
     
     DBUSER = "SELECT id, first, last, age FROM DBUSER";
     ResultSet rs = stmt.executeQuery(DBUSER);

     while(rs.next()){
        
        int id  = rs.getInt("id");
        int age = rs.getInt("age");
        String first = rs.getString("first");
        String last = rs.getString("last");

        
        System.out.print("ID: " + id);
        System.out.print(", Age: " + age);
        System.out.print(", First: " + first);
        System.out.println(", Last: " + last);
     }
     rs.close();
  }catch(SQLException se){
    
     se.printStackTrace();
  }catch(Exception e){
    
     e.printStackTrace();
  }finally{
    
     try{
        if(stmt!=null)
           conn.close();
     }catch(SQLException se){
     }
     try{
        if(conn!=null)
           conn.close();
     }catch(SQLException se){
        se.printStackTrace();
     }
  }
  System.out.println("Goodbye!");
}
*/	      
	    //DELETE	      
	      String DBUSER = "DELETE FROM DBUSER " +
                  "WHERE id = 2";
	      stmt.executeUpdate(DBUSER);
	      DBUSER = "SELECT id, first, last, age FROM DBUSER";
	      ResultSet rs = stmt.executeQuery(DBUSER);

	      while(rs.next()){
	         
	         int id  = rs.getInt("id");
	         int age = rs.getInt("age");
	         String first = rs.getString("first");
	         String last = rs.getString("last");

	        
	         System.out.print("ID: " + id);
	         System.out.print(", Age: " + age);
	         System.out.print(", First: " + first);
	         System.out.println(", Last: " + last);
	      }
	      rs.close();
	   }catch(SQLException se){
	      
	      se.printStackTrace();
	   }catch(Exception e){
	      
	      e.printStackTrace();
	   }finally{
	      
	      try{
	         if(stmt!=null)
	            conn.close();
	      }catch(SQLException se){
	      }
	      try{
	         if(conn!=null)
	            conn.close();
	      }catch(SQLException se){
	         se.printStackTrace();
	      }
	   }
	   System.out.println("Goodbye!");
	}
}

