package CreateTable;
import java.sql.DriverManager;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Locale;

public class CreateTable {
	private static final String DB_DRIVER = "oracle.jdbc.driver.OracleDriver";
	private static final String DB_CONNECTION = "jdbc:oracle:thin:@localhost:1521:XE";
	private static final String DB_USER = "USER_ZONA_51";
	private static final String DB_PASSWORD = "masterzonu51";

	public static void main(String[] argv) {

		try {
			createDbUserTable();
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
	}

	private static void createDbUserTable() throws SQLException {

		Connection dbConnection = null;
		Statement statement = null;

		String DBUSER = "CREATE TABLE DBUSER" + "(id INTEGER not NULL, "
				+ " grupa INTEGER, " + " first VARCHAR(255), "
				+ " last VARCHAR(255), " + " age INTEGER, "
				+ " PRIMARY KEY ( grupa ))";
		String DBGROUP = "CREATE TABLE DBGROUP" + "(id INTEGER not NULL, "
				+ " name VARCHAR(255), " + " PRIMARY KEY ( id ))";

		try {
			dbConnection = getDBConnection();
			statement = dbConnection.createStatement();
			System.out.println(DBUSER);
			System.out.println(DBGROUP);

			statement.execute(DBUSER);
			statement.execute(DBGROUP);

			System.out.println("Table \"DBUSER\" is created!");
			System.out.println("Table \"DBGROUP\" is created!");

		} catch (SQLException e) {
			System.out.println(e.getMessage());

		} finally {

			if (statement != null) {
				statement.close();
			}

			if (dbConnection != null) {
				dbConnection.close();
			}
		}
	}

	private static Connection getDBConnection() {

		Connection dbConnection = null;
		try {
			Class.forName(DB_DRIVER);
			Locale.setDefault(Locale.ENGLISH);
		} catch (ClassNotFoundException e) {
			System.out.println(e.getMessage());

		}

		try {
			dbConnection = DriverManager.getConnection(DB_CONNECTION, DB_USER,
					DB_PASSWORD);
			return dbConnection;

		} catch (SQLException e) {
			System.out.println(e.getMessage());

		}
		return dbConnection;
	}
}
