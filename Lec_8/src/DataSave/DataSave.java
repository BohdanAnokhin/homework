package DataSave;

import java.sql.DriverManager;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Locale;

public class DataSave {
	static final String JDBC_DRIVER = "oracle.jdbc.driver.OracleDriver";
	static final String DB_URL = "dbc:oracle:thin:@localhost:1521:XE";
	static final String USER = "USER_ZONA_51";
	static final String PASS = "masterzonu51";

	public static void main(String[] args) {
		Connection conn = null;
		Statement stmt = null;
		try {

			Class.forName("oracle.jdbc.driver.OracleDriver");
			Locale.setDefault(Locale.ENGLISH);

			System.out.println("Connecting to a selected database...");
			conn = DriverManager.getConnection(DB_URL, USER, PASS);
			System.out.println("Connected database successfully...");

			System.out.println("Inserting records into the table...");
			stmt = conn.createStatement();

			String DBGROUP = "INSERT INTO DBGROUP " + "VALUES (114, 'PZ')";
			stmt.executeUpdate(DBGROUP);
			DBGROUP = "INSERT INTO DBGROUP " + "VALUES (115, 'KM')";
			stmt.executeUpdate(DBGROUP);
			DBGROUP = "INSERT INTO DBGROUP " + "VALUES (116, 'SP')";
			stmt.executeUpdate(DBGROUP);
			DBGROUP = "INSERT INTO DBGROUP " + "VALUES(117, 'SKS')";
			stmt.executeUpdate(DBGROUP);

			String DBUSER = "INSERT INTO DBUSER "
					+ "VALUES (001, 114, 'Bohdan', 'Ivanov', 18)";
			stmt.executeUpdate(DBUSER);
			DBUSER = "INSERT INTO DBUSER "
					+ "VALUES (002, 115, 'Dinis', 'Ivanov', 18)";
			stmt.executeUpdate(DBUSER);
			DBUSER = "INSERT INTO DBUSER "
					+ "VALUES (003, 116, 'Ivan', 'Ivanov', 18)";
			stmt.executeUpdate(DBUSER);
			DBUSER = "INSERT INTO DBUSER "
					+ "VALUES(004, 117, 'Evhen', 'Ivanov', 18)";
			stmt.executeUpdate(DBUSER);

			System.out.println("Inserted records into the table...");

		} catch (SQLException se) {
			
			se.printStackTrace();
		} catch (Exception e) {
			
			e.printStackTrace();
		} finally {
			
			try {
				if (stmt != null)
					conn.close();
			} catch (SQLException se) {
			}
			try {
				if (conn != null)
					conn.close();
			} catch (SQLException se) {
				se.printStackTrace();
			}
		}
		System.out.println("Goodbye!");
	}
}
