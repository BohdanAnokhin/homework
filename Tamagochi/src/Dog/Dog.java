package Dog;

import Pet.Pet;

public class Dog implements Pet {

	private int eatValue = 100;
	private int sleepValue = 100;
	public int fatigue = 100;

	private void sayGav() {
		System.out.println("Gav");
		sleepValue = sleepValue - 20;
		eatValue = eatValue - 10;
		fatigue = fatigue - 20;
	}

	private void eat() {
		if (eatValue <= 81) {
			System.out.println("I have eaten");
			eatValue = eatValue + 19;
			if (fatigue <= 85) {
				fatigue = fatigue + 15;
			}
			sleepValue = sleepValue - 15;
		} else {
			System.out.println("I do not want to eat");
		}
	}

	private void sleep() {
		if (sleepValue <= 80) {
			System.out.println("Sleep");
			System.out.println("Woke");
			sleepValue = sleepValue + 30;
			eatValue = eatValue - 20;
			if (fatigue <= 90) {
				fatigue = fatigue + 10;
			}
		} else {
			System.out.println("I do not want to sleep");
		}

	}

	private void playGame() {
		System.out.println("GAME OVER");
		fatigue = fatigue - 40;
		eatValue = eatValue - 30;
		sleepValue = sleepValue - 30;
	}

	public int getFatigue() {
		return fatigue;
	}

	public void startMethod(int m) {
		switch (m) {
		case 1:
			sayGav();
			break;
		case 2:
			eat();
			break;
		case 3:
			sleep();
			break;
		case 4:
			playGame();
			break;
		case 5:
			System.exit(0);
			break;
		}
		startInf();
	}

	public void startInf() {
		System.out.println("sleep = " + sleepValue + "%;" + " eat = "
				+ eatValue + "%;" + " fatigue " + fatigue + "%");
	}
}
