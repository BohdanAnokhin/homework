import java.io.*;
import java.util.Scanner;

public class Pet {
	private int eatValue = 100;
	private int sleepValue = 100;
	public int fatigue = 100;

	private void speak() {
		System.out.println("voice");
		sleepValue = sleepValue - 20;
		eatValue = eatValue - 10;
		fatigue = fatigue - 20;
	}

	private void eat() {
		if (eatValue <= 80) {
			System.out.println("I have eaten");
			eatValue = eatValue + 20;
			if (fatigue <= 90) {
				fatigue = fatigue + 10;
			}
			sleepValue = sleepValue - 20;
		} else {
			System.out.println("I do not want to eat");
		}
	}

	private void sleep() {
		if (sleepValue <= 80) {
			System.out.println("Sleep");
			System.out.println("Woke");
			sleepValue = sleepValue + 20;
			eatValue = eatValue - 20;
			if (fatigue <= 90) {
				fatigue = fatigue + 10;
			}
		} else {
			System.out.println("I do not want to sleep");
		}

	}

	private void playGame() {
		System.out.println("GAME OVER");
		fatigue = fatigue - 40;
		eatValue = eatValue - 30;
		sleepValue = sleepValue - 30;
	}

	public int getFatigue() {
		return fatigue;
	}

	public void startMethod(int m) throws Exception {
		switch (m) {
		case 1:
			speak();
			break;
		case 2:
			eat();
			break;
		case 3:
			sleep();
			break;
		case 4:
			playGame();
			break;
		case 5:
			option();
			break;
		case 6:
			System.exit(0);
			break;
		}
		startInf();

	}

	private void option() throws Exception {
		System.out
				.println("1 - new game, 2 - save, 3 - save_in_file, 4 - open_config");
		int met = new Scanner(System.in).nextInt();
		if (met == 1 || met == 2 || met == 3 || met == 4) {
			startMethod1(met);

		}

	}

	private void startMethod1(int m1) throws Exception {
		switch (m1) {
		case 1:
			new_game();
			break;
		case 2:
			save();
			break;
		case 3:
			save_in_file();
			break;
		case 4:
			openconfig();
			break;
		case 5:
			System.exit(0);
			break;
		}

	}

	private void save_in_file() {
		System.out.println("specify the path to save: ");
		Scanner newscan = new Scanner(System.in);
		String input;
		input = newscan.nextLine();

		try (FileWriter writer = new FileWriter(input)) {
			char[] buffer = new char[(int) input.length()];

			writer.write("sleep = " + sleepValue + "%;" + " eat = " + eatValue
					+ "%;" + " fatigue " + fatigue + "%");
			// System.out.println(new String(buffer));
		} catch (IOException ex) {

			System.out.println(ex.getMessage());
		}
		System.out.println("Done");

	}

	private void openconfig() throws Exception {

		File f = new File("D:/f1.txt");
		try (FileReader reader = new FileReader(f)) {
			char[] buffer = new char[(int) f.length()];
			// ������� ���� ���������
			reader.read(buffer);
			System.out.println(new String(buffer));
		} catch (IOException ex) {

			System.out.println(ex.getMessage());
		}

	}

	private void save() {
		try {
			File f1 = new File("D:/f1.txt");
			if (f1.createNewFile())

				try (FileWriter writer = new FileWriter(f1)) {
					char[] buffer = new char[(int) f1.length()];

					writer.write("sleep = " + sleepValue + "%;" + " eat = "
							+ eatValue + "%;" + " fatigue " + fatigue + "%");
					System.out.println(new String(buffer));
				} catch (IOException ex) {

					System.out.println(ex.getMessage());
				}
			System.out.println("Done");

		} catch (IOException ioe) {
			ioe.printStackTrace();
		}

	}

	private void new_game() {

		System.out.println("22222");

	}

	public void startInf() {
		System.out.println("sleep = " + sleepValue + "%;" + " eat = "
				+ eatValue + "%;" + " fatigue " + fatigue + "%");
	}
}
