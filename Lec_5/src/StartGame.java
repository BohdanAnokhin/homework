import java.util.Scanner;

public class StartGame {
	private static Pet pet;
	private static int petNumber;

	public static void main(String[] args) throws Throwable {

		System.out.println("Select the Pet : 1 - Cat 2 - Dog ");
		Scanner scan = new Scanner(System.in);
		boolean b = true;
		while (b) {
			petNumber = scan.nextInt();
			if (petNumber > 2) {
				System.out.println("Select the correct number");
			} else {
				b = false;
			}
		}

		if (petNumber == 1) {
			System.out.println("You chose the Cat");
			pet = new Pet();

		} else {
			System.out.println("You chose the Dog");
			pet = new Pet();
		}

		pet.startInf();
		while (true) {
			System.out
					.println("1 - say, 2 - eat, 3 - sleep, 4 - play, 5 - option");
			int met = new Scanner(System.in).nextInt();
			if (met == 1 || met == 2 || met == 3 || met == 4 || met == 5) {
				pet.startMethod(met);

			}

			else {
				System.out.println("No such action!");
			}
			if (pet.getFatigue() <= 0) {
				System.out.println("Your animal was taken to the hospital");
				System.out.println("GAME OVER");
				System.exit(0);
			}
		}

	}
}
