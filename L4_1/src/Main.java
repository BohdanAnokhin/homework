import java.util.HashSet;
import java.util.Set;

public class Main {
	public static void main(String[] args) {

		Set<Integer> a = new HashSet<>();
		Set<Integer> b = new HashSet<>();
		a.add(1);
		a.add(2);
		a.add(3);
		a.add(4);

		b.add(1);
		b.add(2);
		b.add(3);

		Operations operations = new Operations() {

			public boolean equals(Set a, Set b) {
				if (a.size() != b.size()) {
					return false;
				}

				if (a.containsAll(b) && b.containsAll(a)) {
					return true;
				}

				return false;
			}

			public Set union(Set a, Set b) {

				Set c = new HashSet();
				c.addAll(a);
				c.addAll(b);
				return c;
			}

			public Set subtract(Set a, Set b) {

				Set c = new HashSet();
				c.addAll(a);
				c.removeAll(b);
				return c;
			}

			public Set intersect(Set a, Set b) {

				Set c = new HashSet();
				c.addAll(a);
				c.retainAll(b);
				return c;
			}

			public Set symetrikSubtract(Set a, Set b) {

				Set c = new HashSet();
				Set d = new HashSet();
				c.addAll(a);
				d.addAll(b);
				c.removeAll(b);
				d.removeAll(a);
				c.addAll(d);

				return c;
			}
		};

		System.out.println(operations.equals(a, b));
		System.out.println(operations.union(a, b));
		System.out.println(operations.subtract(a, b));
		System.out.println(operations.intersect(a, b));
		System.out.println(operations.symetrikSubtract(a, b));
	}
}
