package arraySort;

import java.util.Arrays;

public class SortArray {
	public static void main(String[] args) {

		int Arr[] = { 2, 1, 9, 6, 4, 7, 3 };
		System.out.println("The not sorted array is:");
		for (int number : Arr) {
			System.out.println("Number = " + number);

		}
		Arrays.sort(Arr);

		System.out.println("The sorted array is:");
		for (int number : Arr) {
			System.out.println("Number = " + number);
		}
	}
}
