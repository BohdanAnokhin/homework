package concatString;

public class Test {
	public static void main(String[] args) {
		try {
			test(new StringBuffer(""));
			test(new StringBuilder(""));
		} catch (java.io.IOException e) {
			System.err.println(e.getMessage());
		}
	}

	private static void test(Appendable obj) throws java.io.IOException {

		long before = System.currentTimeMillis();
		for (int i = 0; i++ < 1e6;) {
			obj.append("");
		}
		long after = System.currentTimeMillis();
		System.out.println(obj.getClass().getSimpleName() + ": "
				+ (after - before) + "ms.");
	}
}
